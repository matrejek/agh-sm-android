//
//  ViewController.h
//  MQTTTry
//
//  Created by Matrejek, Mateusz on 08/04/15.
//  Copyright (c) 2015 Mateusz Matrejek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *msgInput;
@property (weak, nonatomic) IBOutlet UITextField *loginInput;
@property (weak, nonatomic) IBOutlet UITextField *ipInput;
@property (weak, nonatomic) IBOutlet UITableView *listView;

@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UIButton *cnctBtn;

@end
