// ISimpleRPNService.aidl
package pl.edu.agh.sm.rpncalculator;

interface ISimpleRPNService {

	double getStackX();
	double getStackY();
	double getStackZ();
	double getStackT();

	boolean isStackX();
	boolean isStackY();
	boolean isStackZ();
	boolean isStackT();

	void modifyStackX(double value);

	void clear();

	void pressEnter();
	void pressAdd();
	void pressSub();
	void pressDiv();
	void pressMul();
}
