package pl.edu.agh.sm.rpncalculator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CalculatorActivity extends ActionBarActivity {

    private ISimpleRPNService rpnService;
    private boolean bound = false;

    private final static int MENU_CLEAR = 0;
    private final static int MENU_HELP = 1;

    private EditText labelT;
    private EditText labelZ;
    private EditText labelY;
    private EditText labelX;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bound){
            bound = false;
            this.unbindService(rpnConnection);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!bound) {
            this.bindService(new Intent(CalculatorActivity.this,
                            SimpleRPNService.class), rpnConnection,
                    Context.BIND_AUTO_CREATE);
        }

        labelT = (EditText)findViewById(R.id.EditTextStackT);
        labelZ = (EditText)findViewById(R.id.EditTextStackZ);
        labelY = (EditText)findViewById(R.id.EditTextStackY);
        labelX = (EditText)findViewById(R.id.EditTextStackX);

        labelX.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    rpnService.modifyStackX(Double.parseDouble(s.toString()));
                }catch (NumberFormatException e){
                    return;
                }catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private ServiceConnection rpnConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,IBinder service) {
            rpnService = ISimpleRPNService.Stub.asInterface(service);
            bound = true;
            Toast.makeText(CalculatorActivity.this, "RPN Service Connected!"
                    , Toast.LENGTH_SHORT).show();
        }
        public void onServiceDisconnected(ComponentName className) {
            rpnService = null;
            bound = false;
            Toast.makeText(CalculatorActivity.this, "RPN Service Disconnected!"
                    , Toast.LENGTH_SHORT).show();
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_calculator, menu);
        menu.add(0, MENU_CLEAR, 0, R.string.clear);
        menu.add(0, MENU_HELP, 0, R.string.help);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){

            case MENU_CLEAR:

                try {
                    rpnService.clear();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                refresh();
                return true;
            case MENU_HELP:

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(!isFinishing()){
                            new AlertDialog.Builder(CalculatorActivity.this)
                                    .setTitle("Help Dialog")
                                    .setMessage("There is no hope for help")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // whatever...
                                        }
                                    }).create().show();
                        }
                    }
                });

                return true;


        }

        return super.onOptionsItemSelected(item);
    }

    public void mulOnClick(View v) {
        try {
            rpnService.pressMul();
            refresh();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void divOnClick(View v) {
        try {
            rpnService.pressDiv();
            refresh();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void addOnClick(View v) {
        try {
            rpnService.pressAdd();
            refresh();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void subOnClick(View v) {
        try {
            rpnService.pressSub();
            refresh();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    public void enterOnClick(View v) {
        try {
            rpnService.pressEnter();
            refresh();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void refresh() {


        try {

            final String empty = getResources().getString(R.string.empty);
            final String x = !rpnService.isStackX() ? empty : new Double(rpnService.getStackX()).toString();
            final String y = !rpnService.isStackY() ? empty : new Double(rpnService.getStackY()).toString();
            final String z = !rpnService.isStackZ() ? empty : new Double(rpnService.getStackZ()).toString();
            final String t = !rpnService.isStackT() ? empty : new Double(rpnService.getStackT()).toString();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    labelX.setText(x);
                    labelY.setText(y);
                    labelZ.setText(z);
                    labelT.setText(t);
                }
            });


        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
