# README #

### What is this repository for? ###

This repo is mainly for storing Android code I wrote during university classes.

* SMLAB1 is a simple calculator using Reverse Polish Notation (http://en.wikipedia.org/wiki/Reverse_Polish_notation)
* SMLAB2 is a simple weather application
* SMLAB3 is a simple chat based on MQTT protocol
 
### How do I get set up? ###

* All projects here are compatible with Android Studio
* All you need is to import it in Android Studio and provide valid Android SDK for running
