package pl.edu.agh.sm.smlab3;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.ArrayList;
import java.util.List;


public class ChatActivity extends ActionBarActivity implements MqttCallback{

    private String nick;

    private String ip;

    private String broker;

    private String topic;

    int qos = 0;

    List listItems = new ArrayList<String>();

    MemoryPersistence persistence = new MemoryPersistence();

    MqttClient sampleClient=null;

    EditText messageEdit;

    ArrayAdapter<String> adapter;

    private  ListView chatListView;

    public static final String NICK = "NICK";
    public static final String MSG = "MSG";

    Handler myHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            listItems.add("["+msg.getData().getString(NICK) + "]" +
                    msg.getData().getString(MSG));
            adapter.notifyDataSetChanged();
            chatListView.setSelection(listItems.size()-1);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        nick = getIntent().getStringExtra(MainActivity.LOGIN);
        ip = getIntent().getStringExtra(MainActivity.IP);
        messageEdit = (EditText)findViewById(R.id.etMessage);
        //init list
        chatListView = (ListView) findViewById(R.id.chatList);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listItems);
        chatListView.setAdapter(adapter);


        new AsyncTask<Integer, Void, Void>(){
            @Override
            protected Void doInBackground(Integer... params) {
                startMQTT();
                return null;
            }
        }.execute();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void connectionLost(Throwable throwable) {
        Toast.makeText(this, "Connection lost", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        Log.i("ChatActivity","[CallBack->" + topic + "]" + message.toString());
        Message m = myHandler.obtainMessage();
        Bundle b = new Bundle();
        b.putString(MSG, message.toString());
        b.putString(NICK,topic );

        m.setData(b);
        myHandler.sendMessage(m);

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    public void startMQTT(){

        try {
            broker = "tcp://"+ip+":1883";
            topic = nick;
            sampleClient = new MqttClient(broker, nick, persistence);
            sampleClient.setCallback(this);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            Log.i("ChatActivity", "Connecting to broker: " + broker);
            sampleClient.connect(connOpts);
            Log.i("ChatActivity","Connected");
            sampleClient.subscribe("#");
        } catch (MqttException e) {
            Log.e("ChatActivity","Unable to connect: " + e.getMessage());
        }
    }

    public void onButtonClick(View v){


        final String line = messageEdit.getText().toString();
        messageEdit.setText("");

        new AsyncTask<Integer, Void, Void>(){
            @Override
            protected Void doInBackground(Integer... params) {
                MqttMessage message = new MqttMessage(line.getBytes());
                message.setQos(qos);
                try {
                    sampleClient.publish(topic, message);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();



    }
}

