package pl.edu.agh.sm.smlab2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WeatherActivity extends ActionBarActivity {


    public static final String APP_ID = "AmCWlejV34EF2q3SH5l9IxJ5xsgdrEKa4QXQ2yE3DuVFP4.y4sVHVVLQqyJ2fUHfkdURuJhbIrUTcQ--";
    public static final String YAHOO_WOEID_QUERY = "http://where.yahooapis.com/v1/places.q(%f%%20%f)?appid=%s&format=json";
    public static final String WOEID = "woeid";
    public static final String YAHOO_FORECAST_QUERY = "http://weather.yahooapis.com/forecastrss?w=%s&u=c";

    private final Pattern DATA_PATTERN = Pattern.compile(Pattern.quote("<![CDATA[") + "(.*?)" + Pattern.quote("]]>"), Pattern.DOTALL);
    private final Pattern CITY_PATTERN = Pattern.compile("city=\"(.*?)\"");

    protected LocationProvider locationProvider;
    protected LocationManager locationManager;



    WebView weatherWebView;
    TextView cityTextView;
    TextView locationLabel;

    private String lon;
    private String lat;


    Handler weatherHandler = new Handler(){

        public void handleMessage(Message msg) {
            lat = msg.getData().getString("lat");
            lon = msg.getData().getString("lon");
            String web = msg.getData().getString("web");
            String city = msg.getData().getString("city");
            locationLabel.setText(String.format("(%.3f , %.3f)",new Double(lat), new Double(lon)));
            cityTextView.setText(city);
            weatherWebView.loadDataWithBaseURL(null, web, "text/html", "utf-8", null);
        }
    };

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            final double lat = (location.getLatitude());
            final double lon = location.getLongitude();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    updateWeather(lat,lon);
                }
            }).start();
        };

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    protected void onStop(){
        super.onStop();
        if (locationProvider != null){
            Toast.makeText(this, "Location listener unregistered!", Toast.LENGTH_SHORT).show();
            this.locationManager.removeUpdates(this.locationListener);
        }
    }
    protected void onStart() {
        super.onStart();
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        locationProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);

        if (locationProvider != null){
            Toast.makeText(this, "Location listener registered!", Toast.LENGTH_SHORT).show();
            this.locationManager.requestLocationUpdates(locationProvider.getName(),
                    0, 0, this.locationListener);
        } else {
            Toast.makeText(this, "Location Provider missing!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        weatherWebView = (WebView)findViewById(R.id.webView);
        cityTextView = (TextView)findViewById(R.id.cityView);
        locationLabel = (TextView) findViewById(R.id.coordinatesView);
        weatherWebView.setBackgroundColor(Color.TRANSPARENT);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_weather, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onButtonClick(View v){
        Intent myIntent = new Intent(Intent.ACTION_VIEW,Uri.parse("geo:" + lat + "," + lon + "?z=14"));
        startActivity(myIntent);
    }

    private void updateWeather(double lat, double lon){
        String woeid = getWOEID(lat, lon);
        String rss = getRSS(woeid);
        String weather = getWeather(rss);
        String city = getCity(rss);

        Message m = weatherHandler.obtainMessage();
        Bundle b = new Bundle();
        b.putString("lat", String.valueOf(lat));
        b.putString("lon", String.valueOf(lon));
        b.putString("woeid", woeid);
        b.putString("web", weather);
        b.putString("city", city);
        m.setData(b);
        weatherHandler.sendMessage(m);
    }


    private String getWeather(String rss){
        Matcher matcher = DATA_PATTERN.matcher(rss);
        if(matcher.find()) {
            return  matcher.group(1);
        }else{
            return getResources().getString(R.string.weatherUnavailable);
        }
    }


    private String getCity(String rss){
        Matcher matcher = CITY_PATTERN.matcher(rss);
        if(matcher.find()) {
            return  matcher.group(1);
        }else{
            return getResources().getString(R.string.Unknown);
        }
    }

    private String getWOEID(double lat, double lon) {
        String woeid = "";

        String json_ret = getContentFromUrl(String.format(YAHOO_WOEID_QUERY, lat, lon, APP_ID));

        if (json_ret==null){
            return "";
        }

        try {
            JSONObject jsonObject = new JSONObject(json_ret);
           jsonObject = new JSONObject( jsonObject.get("places").toString());
            JSONArray array = jsonObject.getJSONArray("place");
           woeid = new JSONObject(array.get(0).toString()).getString(WOEID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.v("WOEID",woeid);
        return woeid;
    }

    private String getRSS(String woeid){
        String rss = getContentFromUrl(String.format(YAHOO_FORECAST_QUERY,woeid));
        return rss;
    }

    public String getContentFromUrl(String url) {
        String content = null;

        Log.v("[GEO WEATHER ACTIVITY]", url);

        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpPost = new HttpGet(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            content = EntityUtils.toString(httpEntity);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
